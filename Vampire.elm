module Vampire exposing (..)

import Html exposing (..)
import Html.App as App
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import VtmData


-- MAIN


main : Program Never
main =
    App.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { name : String
    , player : String
    , clan : String
    }


init : ( Model, Cmd a )
init =
    let
        initModel =
            { name = ""
            , player = ""
            , clan = ""
            }
    in
        ( initModel, Cmd.none )



-- UPDATE


type Msg
    = NoOp
    | NameInput String
    | PlayerInput String
    | ClanSelect String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        NameInput str ->
            ( { model | name = str }, Cmd.none )

        PlayerInput str ->
            ( { model | player = str }, Cmd.none )

        ClanSelect str ->
            ( { model | clan = str }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


viewName : Model -> Html Msg
viewName model =
    let
        nameInput =
            input
                [ value model.name
                , onInput NameInput
                , placeholder "Character Name"
                ]
                []
    in
        span [ class "name-div" ] [ nameInput ]


viewPlayer : Model -> Html Msg
viewPlayer model =
    let
        playerInput =
            input
                [ value model.player
                , onInput PlayerInput
                , placeholder "Player Name"
                ]
                []
    in
        span [ class "player-div" ] [ playerInput ]


viewClan : Model -> Html Msg
viewClan model =
    let
        clanSelect =
            select
                [ onInput ClanSelect ]
                (List.map (\c -> option [] [ text c ]) VtmData.clans)
    in
        span [ class "clan-div" ] [ clanSelect ]


view : Model -> Html Msg
view model =
    div []
        [ span [] [ viewName model, viewPlayer model ]
        , div [] [ viewClan model ]
        ]
