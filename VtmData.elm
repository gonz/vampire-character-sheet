module VtmData exposing (..)


clans : List String
clans =
    [ "Ventrue"
    , "Tremere"
    , "Toreador"
    , "Nosferatu"
    , "Malkavian"
    , "Brujah"
    , "Gangrel"
    ]


type Clan
    = Ventrue
    | Tremere
    | Toreador
    | Malkavian
    | Brujah
    | Gangrel
